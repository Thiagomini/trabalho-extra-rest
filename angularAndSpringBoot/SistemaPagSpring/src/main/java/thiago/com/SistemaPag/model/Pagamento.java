/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thiago.com.SistemaPag.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Thiago
 */

@Entity
@Table(name = "pagamentos")
public class Pagamento implements Serializable, Cloneable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ID;
    
    
    @PositiveOrZero (message = "Valor do pagamento deve ser positivo!")
    private double valor;
    private boolean aprovado;
    
    @NotEmpty(message = "Descricao do pagamento não pode ser nula!")
    private String descricao;
    private String detalhes;
    private String motivoRejeicao;
    
    private LocalDate dataVencimento;
    private LocalDate dataAprovacao;

    @ManyToOne
    private Employee solicitante;

    public Pagamento() {
    }

    public Pagamento(double valor, String descricao, Employee solicitante) {
        this.valor = valor;
        this.descricao = descricao;
        this.solicitante = solicitante;
        this.aprovado = false;
    }

      
    
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
      
      
      
    public String getMotivoRejeicao() {
        return motivoRejeicao;
    }

    public void setMotivoRejeicao(String motivoRejeicao) {
        this.motivoRejeicao = motivoRejeicao;
    }

    public LocalDate getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(LocalDate dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public LocalDate getDataAprovacao() {
        return dataAprovacao;
    }

    public void setDataAprovacao(LocalDate dataAprovacao) {
        this.dataAprovacao = dataAprovacao;
    }
    
  
    
    public Employee getEmployee() {
        return solicitante;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isAprovado() {
        return aprovado;
    }

    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public Employee getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Employee solicitante) {
        this.solicitante = solicitante;
    }
    
   
    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
    
    @Override
    public String toString() {
        return "Pagamento [id=" + ID + ", valor=" + valor + ", descricao=" + descricao + ", solicitante="+ solicitante + ", dataVencimento=" + dataVencimento
       + "]";
    }
    
    public static void clonePagamento(Pagamento p, Pagamento clone) {
        
        clone.setValor(p.getValor());
        clone.setDescricao(p.getDescricao());
        clone.setSolicitante(p.getSolicitante());
        clone.setAprovado(p.isAprovado());
        clone.setDataAprovacao(p.getDataAprovacao());
        clone.setDataVencimento(p.getDataVencimento());
        clone.setDetalhes(p.getDetalhes());
        clone.setMotivoRejeicao(p.getMotivoRejeicao());
        
    }
}
