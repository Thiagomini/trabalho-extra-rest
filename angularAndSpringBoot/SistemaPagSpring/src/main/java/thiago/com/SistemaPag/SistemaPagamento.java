package thiago.com.SistemaPag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaPagamento {

	public static void main(String[] args) {
		SpringApplication.run(SistemaPagamento.class, args);
	}

}
