/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thiago.com.SistemaPag.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import thiago.com.SistemaPag.exception.ResourceNotFoundException;
import thiago.com.SistemaPag.model.Pagamento;
import thiago.com.SistemaPag.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Thiago
 */

@CrossOrigin(origins = "http://localhost:4200")
@RestController 
@RequestMapping("/api/v1")
public class PagamentoController {
    @Autowired
    private PagamentoRepository pagamentoRepository;
    
    @GetMapping("/pagamentos")
    public List<Pagamento> getAllPagamentos() {
        return pagamentoRepository.findAll();
    }
    
    @GetMapping("/pagamentos/{id}")
    public ResponseEntity<Pagamento> getPagamentoById(@PathVariable(value = "id") Long pagamentoId)
        throws ResourceNotFoundException {
        Pagamento pagamento = pagamentoRepository.findById(pagamentoId)
          .orElseThrow(() -> new ResourceNotFoundException("Pagamento not found for this id :: " + pagamentoId));
        return ResponseEntity.ok().body(pagamento);
    }
    
    
    @PostMapping("/pagamentos")
    public Pagamento createPagamento(@Valid @RequestBody Pagamento pagamento) {
       
        return pagamentoRepository.save(pagamento);
    }

    @PutMapping("/pagamentos/{id}")
    public ResponseEntity<Pagamento> updatePagamento(@PathVariable(value = "id") Long pagamentoId,
         @Valid @RequestBody Pagamento pagamentoDetails) throws ResourceNotFoundException {
        Pagamento pagamento = pagamentoRepository.findById(pagamentoId)
        .orElseThrow(() -> new ResourceNotFoundException("Pagamento not found for this id :: " + pagamentoId));

        Pagamento.clonePagamento(pagamentoDetails, pagamento);
        final Pagamento updatedPagamento = pagamentoRepository.save(pagamento);
        return ResponseEntity.ok(updatedPagamento);
    }

    @DeleteMapping("/pagamentos/{id}")
    public Map<String, Boolean> deletePagamento(@PathVariable(value = "id") Long pagamentoId)
         throws ResourceNotFoundException {
        Pagamento pagamento = pagamentoRepository.findById(pagamentoId)
       .orElseThrow(() -> new ResourceNotFoundException("Pagamento not found for this id :: " + pagamentoId));

        pagamentoRepository.delete(pagamento);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
