/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thiago.com.SistemaPag.repository;

import thiago.com.SistemaPag.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Thiago
 */
public interface PagamentoRepository extends JpaRepository<Pagamento, Long>{
    
}
