/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thiago.com.SistemaPag.model;

/**
 *
 * @author Thiago
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employee implements Serializable {

    private long id;
    private String nome;
    private String cargo;
    private String emailId;
 
    public Employee() {
  
    }
 
    public Employee(String nome, String cargo, String emailId) {
         this.nome = nome;
         this.cargo = cargo;
         this.emailId = emailId;
    }
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
        public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
 
    @Column(name = "nome", nullable = false)
    public String getNome() {
        return nome;
    }
    public void setNome(String firstName) {
        this.nome = firstName;
    }
 
    @Column(name = "cargo", nullable = false)
    public String getCargo() {
        return cargo;
    }
    public void setCargo(String lastName) {
        this.cargo = lastName;
    }
 
    @Column(name = "email_address", nullable = false)
    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public String toString() {
        return "Empregado [id=" + id + ", nome=" + nome + ", cargo=" + cargo + ", emailId=" + emailId
       + "]";
    }
 
}
