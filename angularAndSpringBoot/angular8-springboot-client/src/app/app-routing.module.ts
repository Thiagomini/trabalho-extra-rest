import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';

import { PagamentoDetailsComponent } from './pagamento-details/pagamento-details.component';
import { CreatePagamentoComponent } from './create-pagamento/create-pagamento.component';
import { PagamentoListComponent } from './pagamento-list/pagamento-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'employee', pathMatch: 'full' },
  { path: 'employees', component: EmployeeListComponent },
  { path: 'add', component: CreateEmployeeComponent },
  { path: 'details/:id', component: EmployeeDetailsComponent },
  { path: 'pagamentos', component: PagamentoListComponent },
  { path: 'novoPagamento', component: CreatePagamentoComponent },
  { path: 'pagamentos/details/:id', component: PagamentoDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
