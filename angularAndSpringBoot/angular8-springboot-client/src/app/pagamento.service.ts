import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PagamentoService {

  private baseUrl = 'http://localhost:8080/api/v1/pagamentos';

  constructor(private http: HttpClient) { }

  getPagamento(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createPagamento(pagamento: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, pagamento);
  }

  updatePagamento(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deletePagamento(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getPagamentosList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
