import { Employee } from "./employee";
export class Pagamento {
  id: number;
  valor: number;
  aprovado: boolean;
  descricao: string;
  detalhes: string;
  idSolicitante: number;
  solicitante: Employee;
  dataVencimento: Date;
  dataAprovacao: Date;
  motivoRejeicao: string;
}
