import { PagamentoService } from '../pagamento.service';
import { EmployeeService } from '../employee.service';
import { Pagamento } from '../pagamento';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { Employee } from "../employee";
@Component({
  selector: 'app-create-pagamento',
  templateUrl: './create-pagamento.component.html',
  styleUrls: ['./create-pagamento.component.css']
})
export class CreatePagamentoComponent implements OnInit {

  pagamento: Pagamento = new Pagamento();
  submitted = false;
  employees: Observable<Employee[]>;

  constructor(private pagamentoService: PagamentoService, private employeeService:EmployeeService,
    private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.employees = this.employeeService.getEmployeesList();
  }

  newPagamento(): void {
    this.submitted = false;
    this.pagamento = new Pagamento();
  }

  save() {

  var sol = new Employee();
    this.employeeService.getEmployee(this.pagamento.idSolicitante).
        subscribe(data => {
          sol.id = data.id;
          sol.nome = data.nome;
          sol.cargo = data.cargo;
          sol.emailId = data.emailId;
          console.log("Id do Solicitante: "+sol.id);
          this.pagamento.solicitante = sol;
          this.pagamentoService.createPagamento(this.pagamento)
            .subscribe(data => console.log(data), error => console.log(error));
          this.pagamento = new Pagamento();
          this.gotoList();
        });

  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/pagamentos']);
  }
}
