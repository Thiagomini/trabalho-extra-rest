import { PagamentoDetailsComponent } from '../pagamento-details/pagamento-details.component';
import { Observable } from "rxjs";
import { PagamentoService } from "../pagamento.service";
import { Pagamento } from "../pagamento";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: "app-pagamento-list",
  templateUrl: "./pagamento-list.component.html",
  styleUrls: ["./pagamento-list.component.css"]
})
export class PagamentoListComponent implements OnInit {
  pagamentos: Observable<Pagamento[]>;

  constructor(private pagamentoService: PagamentoService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.pagamentos = this.pagamentoService.getPagamentosList();
  }

  deletePagamento(id: number) {
    this.pagamentoService.deletePagamento(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  aprovaPagamento(id:number) {
    
  }

  pagamentoDetails(id: number){
    this.router.navigate(['pagamentos/details/', id]);
  }
}
