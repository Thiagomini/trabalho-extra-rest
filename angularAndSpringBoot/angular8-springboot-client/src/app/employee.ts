export class Employee {
    id: number;
    nome: string;
    cargo: string;
    emailId: string;
    active: boolean;
}
