import { Pagamento } from '../pagamento';
import { Component, OnInit, Input } from '@angular/core';
import { PagamentoService } from '../pagamento.service';
import { PagamentoListComponent } from '../pagamento-list/pagamento-list.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pagamento-details',
  templateUrl: './pagamento-details.component.html',
  styleUrls: ['./pagamento-details.component.css']
})
export class PagamentoDetailsComponent implements OnInit {

  id: number;
  pagamento: Pagamento;

  constructor(private route: ActivatedRoute,private router: Router,
    private pagamentoService: PagamentoService) { }

  ngOnInit() {
    this.pagamento = new Pagamento();

    this.id = this.route.snapshot.params['id'];

    this.pagamentoService.getPagamento(this.id)
      .subscribe(data => {
        console.log(data)
        this.pagamento = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['pagamentos']);
  }
}
